defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  curry,
} from 'ramda'

import {
  pipe, compose, composeRight,
  prop, tap, ok, whenOk, dot, dot1,
  eq, ifFalse, ifTrue, noop,
  whenPredicate, sprintf1,
} from 'stick-js'

const whenEquals = eq | whenPredicate

import React, { PureComponent, Component, } from 'react'

// --- curry is slow -- consider manually currying these.

export const Elem = elem => React.createElement (elem)
export const ElemP = curry ((elem, props) => React.createElement (elem, props))
export const ElemPC = curry ((elem, props, children) => React.createElement (elem, props, children))

// --- e.g.
// --- { results | mapX (wrapWithXP (ResultsRow, null, { width: '20%' })) }}
export const wrapWithXP = curry ((elemName, contents, propsInit, propsMerge, idx) => ElemPC (
  elemName,
  {
    ... propsInit,
    ... propsMerge,
    key: idx,
  },
  contents,
))

export const wrapWithX = curry ((elemName, contents, idx) => wrapWithXP (elemName, contents, {}, {}, idx))

// xxx cond version
export const keyPress = curry ((f, keyName) => prop ('key') >> whenEquals (keyName, f))

// --- returns value.
export const setStateOn = curry ((ctxt, key, value) => value | tap (_ => ctxt.setState ({ [key]: value })))

// --- usage:
// componentWillReceiveProps (nextProps) {
//   whyYouRerender (componentName, this.props, nextProps)
// }

export const whyYouRerender = (componentName, curProps, nextProps) => {
  for (const k in nextProps) {
    if (nextProps [k] !== curProps [k])
      console.log (componentName | sprintf1 ('rerender %s want:'), k)
  }
}

