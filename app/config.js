defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { curry, fromPairs, } from 'ramda'

import {
  pipe, compose, composeRight,
  join, map, sprintfN,
  compact, match, guard, otherwise,
  ifTrue, ifFalse, whenEquals,
  ifPredicate, gt, noop,
  defaultTo, ok, eq, dot, dot1,
  list,
  die,
  divideBy,
  isArray,
  path,
  sprintf1,
  id,
  mapValues,
  tap,
  reduce,
  mergeToM,
} from 'stick-js'

import {
  error,
  ierrorError,
  ierrorDie,
} from './utils/utils-alert'

import {
  ierror,
  memoize,
  ifObject,
  mergeAll,
  singletonArray,
} from './utils/utils'

const { log, } = console
const logWith = (header) => (...args) => log (... [header, ...args])

const min = a => b => Math.min (a, b)
const ifArray = isArray | ifPredicate

export const config = {
  title: 'Geluiden van de Werkvloer',
  images: {
    map: require ('./images/map4.png'),
    infoBg: require ('./images/brushed-concrete-texture-straight.jpg'),
  },
  // show / hide
  animationTime: ['400ms', '400ms'],
  // --- pos [23, 25] means top 23%, left 25%
  data: [
    {
      color: '#ffaa00',
      pos: [57.809, 68.278],
      latlon: [52.360004, 4.9253631],
      data: {
        language: 'eigen',
        profession: 'stratenmaker',
        // soundFile: 'eigen taal',
        soundFile: require ('./audio/TURKSSTR.mp3'),
        locationGeneral: 'Transvaalbuurt',
        locationSpecific: 'Linneausstraat',
      },
    },
    {
      color: '#b0b009',
      pos: [30.2, 57.2],
      latlon: [52.389794, 4.901869],
      data: {
        language: 'Hindoestaans',
        profession: 'metaalbewerker',
        // soundFile: 'hindoestaans metaalbewerker',
        soundFile: require ('./audio/HINDOEST.mp3'),
        locationGeneral: 'Noord',
        locationSpecific: 'Grasweg aan het IJ',
      },
    },
    {
      color: '#c70000',
      pos: [26, 35.9],
      latlon: [52.397102, 4.836187],
      data: {
        language: 'Marokkaans',
        profession: 'sloperij',
        // soundFile: 'marokkaan sloperij fade',
        soundFile: require ('./audio/MAROKAAN.mp3'),
        locationGeneral: 'Suezkanaal Haven West',
        locationSpecific: 'Radarweg bij Suezkanaal',
      },
    },
    {
      color: '#117f1c',
      pos: [53.809, 66.278],
      latlon: [52.357159, 4.927132],
      data: {
        language: 'Berbers',
        profession: 'poelier',
        // soundFile: 'berbersp',
        soundFile: require ('./audio/BERBERSP.mp3'),
        locationGeneral: 'Transvaalbuurt',
        locationSpecific: 'Linneausstraat',
      },
    },
    {
      color: '#ff6f00',
      pos: [82.658, 83.081],
      latlon: [52.3172697,4.9788697],
      data: {
        language: 'Urdu (Pakistaans)',
        profession: 'kleermaker',
        // soundFile: 'urdu kleermaker met fade'
        soundFile: require ('./audio/URDUKLEE.mp3'),
        locationGeneral: 'Kraaiennest Bijlmer',
        locationSpecific: 'Kraaiennest',
      },
    },
    {
      color: '#1e65da',
      pos: [44.571, 67.445],
      latlon: [52.370456, 4.930024],
      data: {
        language: 'Liberiaans',
        profession: 'krantenloper',
        // soundFile: 'liberiaan krantenlopen',
        soundFile: require ('./audio/krantenloper_LIBERIAAN.mp3'),
        locationGeneral: 'Czaar Peterstraat',
        locationSpecific: 'Czaar Peterstraat',
      },
    },
    {
      color: '#ff3400',
      pos: [54.133, 67.328],
      latlon: [52.360126, 4.928468],
      data: {
        language: 'Surinaams',
        profession: 'cafebaas',
        // soundFile: 'surinaams cafe met fade',
        soundFile: require ('./audio/SURINAAM.mp3'),
        locationGeneral: 'Oost',
        locationSpecific: 'Domselaerstaat bij Muiderpoortstation',
      },
    },
    {
      color: '#f10fa8',
      pos: [35.170, 60.093],
      latlon: [52.386094, 4.911898],
      data: {
        language: 'Italiaans',
        profession: 'ijscoman',
        // soundFile: 'italiaans ijscoman met fade',
        soundFile: require ('./audio/ITALIAAN.mp3'),
        locationGeneral: 'Noord',
        locationSpecific: 'Buiksloterweg bij de pont aan het IJ',
      },
    },
    {
      color: '#0b2fa0',
      pos: [56.240, 61.494],
      latlon: [52.355576, 4.907385],
      data: {
        language: 'Jamaicaans',
        profession: 'krantenverkoper',
        // soundFile: 'jamaica daklozenkrant verkoper',
        soundFile: require ('./audio/JAMAICAD.mp3'),
        locationGeneral: 'Oost',
        locationSpecific: 'Weesperzijde bij de C 1000 Wibautstraat',
      },
    },
    {
      color: '#039633',
      pos: [92.545, 10.852],
      latlon: [52.304099, 4.748003],
      data: {
        language: 'Egyptisch',
        profession: 'beveiliger',
        // soundFile: 'egyptisch beveiliging Schiphol met fade',
        soundFile: require ('./audio/EGYPTISC.mp3'),
        locationGeneral: 'Schiphol',
        locationSpecific: 'Vertrekhal 3',
      },
    },
    {
      color: '#9a0f0f',
      pos: [56.726, 65.928],
      latlon: [52.355255, 4.923623],
      data: {
        language: 'Punjabi',
        profession: 'wasserette',
        // soundFile: 'track 5 punjabi fade out',
        soundFile: require ('./audio/TRACK5PUNJABIwasserette.mp3'),
        locationGeneral: 'Transvaalbuurt',
        locationSpecific: 'Steve Bikoplein',
      },
    },
    {
      color: '#9a0f6d',
      pos: [67.747, 48.775],
      latlon: [52.340269, 4.872388],
      data: {
        language: 'Maleis',
        profession: 'tax manager',
        // soundFile: 'maleisie taxmanager',
        soundFile: require ('./audio/MALEISIE.mp3'),
        locationGeneral: 'Zuid',
        locationSpecific: 'WTC tower D',
      },
    },
    {
      color: '#9a0f6d',
      pos: [33.2, 58.2],
      latlon: [52.392006, 4.899929],
      data: {
        language: 'Marokkaans',
        profession: 'bakker',
        // soundFile: 'marokkaans kwekkeboom, fade',
        soundFile: require ('./audio/MAROKKAA.mp3'),
        locationGeneral: 'Noord Kwekkeboom',
        locationSpecific: 'Grasweg aan het IJ',
      }
    },
  ],

  components: {
    // Main: {},
  }
}

const configGetInOr = curry ((errMsg, thePath) =>
  config | path (thePath) | defaultTo (
    _ => [thePath | join ('/'), errMsg]
      | sprintfN ("Couldn't get config key %s: %s")
      | ierrorDie
  )
)

export const configGetOrDieWith = curry ((errMsg, key) =>
  key | singletonArray | configGetInOr (errMsg)
)

const missingMsg = ifArray (
    join (' → ') >> sprintf1 ("Missing path '%s'"),
                    sprintf1 ("Missing key '%s'"),
)

// xxx configGetOr (defaultVal)

export const configGetOrDie = key => key | configGetOrDieWith (key | missingMsg)

// --- const { ... } = configGetsOrDie ('key1', ..., { key2: ..., key3: ... }, ...)
export const configGetsOrDie = (() => {
  const mapper = ifObject (
    mapValues (configGetOrDie),
    key => ({ [key]: key | configGetOrDie }),
  )

  return list
  >> map (mapper)
  >> mergeAll
}) ()

const componentKeyToPath = (componentKey) => ifArray (
  key => ['components', componentKey, ...key],
  key => ['components', componentKey, key],
)

// const configGetIn = (thePath) =>
//   config | path (thePath) | defaultTo (
//     _ => [thePath | join ('/')]
//       | sprintfN ("Couldn't get config key %s")
//       | ierrorDie
//   )
// export const configGet = ifArray (
//   configGetIn,
//   (key) => [key] | configGetIn
// )
// export const configGetsComponent = curry ((componentKey, keys) =>
//   keys | map (configGetComponent (componentKey))
// )
// export const configGetComponent = curry ((componentKey, key) =>
//   key | componentKeyToPath (componentKey) | configGetIn
// )

// @todo remove
export default config
