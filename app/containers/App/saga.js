defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

const MOCK = true
const MOCKDELAY = 0

const localConfig = {
  verbose: false,
  verkort: 0,
  randomise: false,
}

import {
  curry, drop,
} from 'ramda'

import {
  pipe, compose, composeRight,
  add, multiply, join,
  path,
  sprintf1, sprintfN, ok, dot1, timesF, dot, whenOk,
  compact,
  guard, otherwise,
  exception, raise,
  invoke,
  concatTo,
  lets,
  letS,
  die,
  defaultTo,
  neu1, notOk,
} from 'stick-js'

import { call, put, select, takeLatest, take, } from 'redux-saga/effects'
import request from 'utils/request'

import config from '../../config'

import {
  fetchForecast,
  fetchForecastSuccess,
  fetchForecastError,
  changeLocation,
} from '../App/actions'

// import { } from '../../domains/domain/selectors'

import {
  makeSelectDebug,
} from '../../domains/app/selectors'

import {
  iwarn, isTrue, tellIf,
  resolveP,
  logAndroidPerf,
} from '../../utils/utils.js'

import {
  toJS,
} from '../../utils/utils-immutable'

import {
  error as alertError,
} from '../../utils/utils-alert.js'

import {
  ROUTE_CHANGED,
  FETCH_FORECAST_ERROR,
  CHANGE_LOCATION,
  UPDATE_DEBUG,
  LOG_SERVER,
} from '../../containers/App/constants'

import {
  ierror,
} from '../../utils/utils.js'

const tell = tellIf (localConfig.verbose)

const getTimeStamp = _ => {
  // --- lets has max 6 args for now.
  const d = new Date
  return lets (
    _ => (d.getYear () + 1900) | String,
    _ => (d.getMonth () + 1) | sprintf1 ('%02d'),
    _ => d.getDate () | String,
    _ => (d.getHours ()) | sprintf1 ('%02d'),
    _ => (d.getMinutes ()) | sprintf1 ('%02d'),
    (_, y, m, d, h, mi) => [y, m, d, h, mi] | join (''),
  )
}

const makeRequestURL = (lat, lon) => [lat, lon, getTimeStamp ()]
  | sprintfN ('/api/forecast/json/?lat=%s&lon=%s&btc=%s')

const delayWith = curry ((ret, ms) => new Promise ((res, _) =>
  setTimeout (_ => ret | res, ms))
)

const delay = delayWith (null)

const doRequest = debug => requestURL => debug
  ? call (delayWith, mock (), MOCKDELAY)
  : call (request, requestURL)

function* generalAlert () {
  true | alertError
}

function* sagaFetchForecastError () {
  yield generalAlert | call
}

function* sagaLogServer ({ data: { msg, }}) {
  yield call (request, '/api/log-server', {
    method: 'POST',
    body: JSON.stringify ({ msg }),
    headers: {
      'content-type': 'application/json',
    }
  })
}

function* sagaRouteChange (action) {
  const newLoc = action.payload.pathname | drop (1)
  yield changeLocation (newLoc) | put
}

function* sagaChangeLocation (newLoc) {
  void newLoc

  // --- delay is to try to get the UI (swiping) smoother on mobile.
  yield call (delay, 10)

  yield fetchForecast () | put
}

function* sagaUpdateDebug (debug) {
  yield fetchForecast () | put
}

export default function* rootSaga () {
  yield [
    takeLatest (ROUTE_CHANGED, sagaRouteChange),
    takeLatest (CHANGE_LOCATION, sagaChangeLocation),
    takeLatest (UPDATE_DEBUG, sagaUpdateDebug),
    takeLatest (FETCH_FORECAST_ERROR, sagaFetchForecastError),
    takeLatest (LOG_SERVER, sagaLogServer),
  ]
}

function mock () {
  // --- display edge effect ('atEdge')
  // const basedate = '2018-05-01Z00:15'
  // const basedate = '2018-05-01Z01:15'
  // const basedate = '2018-05-01Z02:15'
  // const basedate = '2018-05-01Z23:15'

  // --- first one that works
  // const basedate = '2018-05-01Z03:15'
  const basedate = '2018-05-01Z03:20'

  // --- work
  // const basedate = '2018-05-01Z07:15'
  // const basedate = '2018-05-01Z11:15'
  // const basedate = '2018-05-01Z13:15'
  // const basedate = '2018-05-01Z19:15'
  // const basedate = '2018-05-01Z21:15'

  const toString = (d) => [
    d.getYear () + 1900,
    d.getMonth () + 1,
    d.getDate (),
    d.getUTCHours (),
    d.getUTCMinutes (),
  ] | sprintfN ('%s-%02d-%02dT%02d:%02d')

  const _basedate = new Date (basedate)
  const mocker = (n) => ({
    utcdatetime: _basedate
      | Number
      | add (n * 1000 * 60 * 5)
      | neu1 (Date)
      | toString,
    precipitation: n
      | multiply (0.2)
      | sprintf1 ('%.1f')
      | Number
  })

  return {
    forecasts: mocker | timesF (34),
  }
}
