/*
 * Clock Messages
 *
 * This contains all the text for the Clock component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Clock.header',
    defaultMessage: 'This is the Clock component !',
  },
});
