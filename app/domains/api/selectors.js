defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { createSelector, } from 'reselect'

import {
  pipe, compose, composeRight,
} from 'stick-js'

import {
  get,
} from '../../utils/utils-immutable'

const selectDomain = get ('api')

export const makeSelectLoading = _ => createSelector (
  selectDomain,
  get ('loading'),
)
