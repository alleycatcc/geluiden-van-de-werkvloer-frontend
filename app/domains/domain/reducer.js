defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { curry, drop, } from 'ramda'

import {
  pipe, compose, composeRight,
  tap, prop, reduce,
  lets,
  neu1, sprintfN,
  appendM,
  xReplace,
} from 'stick-js'

import {
  isNotEmptyObject,
  subtract,
  logAndroidPerf,
  logWith,
} from '../../utils/utils.js'

import {
  toJS, fromJS, getIn,
  listToOrderedSet,
  add as iAdd, del as iDelete,
  set,
} from '../../utils/utils-immutable'

import {
  FETCH_FORECAST,
  FETCH_FORECAST_SUCCESS,
  FETCH_FORECAST_ERROR,
  ROUTE_CHANGED,
  CHANGE_LOCATION,
} from '../../containers/App/constants'

import {
  // makeMakeSelectHasName,
} from './selectors'

const fatalMsg = curry ((msg, state) => state
  | tap (_ => console.error (msg))
  | set ('error') (true)
)

const initialState = fromJS ({
  location: void 8,

  // --- `true` means the reducer is totally corrupted and the app should halt.
  error: false,
  forecast: void 8,
})

// --- hacky am/pm xxx
const formatTimeToAm = date => lets (
  _ => date.getHours (),
  _ => date.getMinutes (),
  (h, m) => [h % 12, m] | sprintfN ('%d:%02d'),
)

import stimeMod from '../../stime'

const stimeFromDateStr = str => stimeMod
  .create ({ segmentLength: 5, })
  .initFromDateStr (str)

// --- tricky: use their 'utcdatetime' property, but first replace T with Z before constructing a
// Date.
const utcDate = xReplace (/T/, 'Z') >> neu1 (Date)

const reducer = (([forecast, forecastByTime], item) => lets (
  _       => item | prop ('utcdatetime') | utcDate,
  (utcdatetime) => lets (
    _ => utcdatetime | stimeFromDateStr,
    (stime) => [stime, stime.getMinutes ()],
  ),
  (utcdatetime) => forecast | appendM ({
    utcdatetime,
    precipitation: item.precipitation,
  }),
  (utcdatetime, [stime, mins]) => forecastByTime.set (mins, {
    mins,
    stime,
    utcdatetime,
    precipitation: item.precipitation,
  }),
  _       => [forecast, forecastByTime],
))

// --- achtung, not point-free
const forecasts = items => items | tap (logWith ('items')) | reduce (reducer, [[], new Map])

// --- some example transforms
// const checkNew = code => getIn (['assignedKeysMeta', code, 'add'])
// const makeCodes = map (prop ('code')) >> listToOrderedSet
// const makeAddedKey = prop ('updateVal') >> (({ missing, active, }) => ({ missing, active, }))
// const makeAssignedKeys = map (({ code, missing, active, }) => [code, { missing, active, }]) >> fromPairs

// --- immutable ??
export default (state = initialState, action) => {
  const { type, data, } = action
  switch (type) {
    case CHANGE_LOCATION:
      logAndroidPerf ('CHANGE_LOCATION in reducer')
      return state
        .set ('location', data)
    case ROUTE_CHANGED:
      logAndroidPerf ('ROUTE_CHANGED in reducer')
      return state
        .set ('location', action.payload.pathname | drop (1))
    case FETCH_FORECAST:
      return state
        .set ('forecast', void 8)
    case FETCH_FORECAST_SUCCESS: {
      const [forecast, forecastByTime] = data | forecasts
      return state
        .set ('forecast', forecast)
        .set ('forecastByTime', forecastByTime)
    }
    default:
      return state
  }
}
