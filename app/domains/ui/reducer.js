defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, whenOk, dot,
} from 'stick-js'

import {
  whenNotEmpty, trim,
} from '../../utils/utils.js'

import {
  toJS, fromJS,
  updateIn,
} from '../../utils/utils-immutable'

import {
  FETCH_FORECAST,
  FETCH_FORECAST_SUCCESS,
  FETCH_FORECAST_ERROR,
} from '../../containers/App/constants'

const initialState = fromJS ({
  assignedKeysLoading: false,
})

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FORECAST:
      return state
        .set ('assignedKeysLoading', true)
    case FETCH_FORECAST_SUCCESS:
      return state
        .set ('assignedKeysLoading', false)
    case FETCH_FORECAST_ERROR:
      return state
        .set ('assignedKeysLoading', false)

    default:
      return state
  }
}
